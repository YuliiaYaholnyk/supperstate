import 'package:mobx/mobx.dart';
import 'package:flutter/material.dart' as Col;

part 'color.g.dart';

class Color = _Color with _$Color;

abstract class _Color with Store {
  @observable
  Col.Color color = Col.Colors.black38;

  @action
  void changeColor(int index) {
      color = Col.Colors.red;
  }

}

//List<Color> colorsList = [
//  Colors.blue,
//  Colors.green,
//  Colors.orange,
//  Colors.pinkAccent,
//  Colors.purple,
//  Colors.deepPurple,
//];
