// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'color.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Color on _Color, Store {
  final _$colorAtom = Atom(name: '_Color.color');

  @override
  Col.Color get color {
    _$colorAtom.reportRead();
    return super.color;
  }

  @override
  set color(Col.Color value) {
    _$colorAtom.reportWrite(value, super.color, () {
      super.color = value;
    });
  }

  final _$_ColorActionController = ActionController(name: '_Color');

  @override
  void changeColor(int index) {
    final _$actionInfo =
        _$_ColorActionController.startAction(name: '_Color.changeColor');
    try {
      return super.changeColor(index);
    } finally {
      _$_ColorActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
color: ${color}
    ''';
  }
}
