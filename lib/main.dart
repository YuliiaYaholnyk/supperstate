
import 'package:cubit/cubit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cubit/flutter_cubit.dart';
import 'package:provider/provider.dart';
import 'package:superStatePractice/provider.dart';

import 'cubit/color_cubit.dart';
import 'cubit/cubit_observer.dart';

//void main() {
//  final store = Store();
//  ColorModule(store);
//  runApp(StoreProvider(
//    store: store,
//    child: MyApp(
//    ),
//  ));
//}
void main() {
//  final store = new Store<AppState>(
//    AppState.reducer,
//    initialState: AppState.initial(),
//    middleware: [
//    ],
//  );
  Cubit.observer = MainCubitObserver();
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(
      create: (_) => ColorProvider(),
    )
  ], child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.grey),
      home: CubitProvider(create: (BuildContext context) => ColorCubit(), child: ColorCubitPage()),
    );
  }
}

class ColorCubitPage extends StatelessWidget {
  Color currentCol;

  @override
  Widget build(BuildContext context) {
    return CubitBuilder<ColorCubit, List<Color>>(builder: (BuildContext context, List<Color> state) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Cubit'),
        ),
        drawer: Drawer(
            child: ListView.builder(
                itemCount: state.length,
                itemBuilder: (BuildContext ctx, int i) {
                  return RaisedButton(
                    color: state[i],
                    onPressed: () => currentCol = state[i],
                    elevation: 5.0,
                  );
                })),
        body: Container(color: currentCol),
      );
    });
  }
}

