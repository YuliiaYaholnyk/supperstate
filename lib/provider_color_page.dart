
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:superStatePractice/provider.dart';



class ProviderColorPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final colorData = Provider.of<ColorProvider>(context);
    return Scaffold(
      appBar: AppBar(title: Text('Provider App')),
           drawer: Drawer(
             child: ListView.builder(
                 itemCount: colorData.colorsList.length,
                 itemBuilder: (BuildContext ctx, int i) {
                   return RaisedButton(
                     color:  colorData.colorsList[i],
                     onPressed: () => colorData.changeColor(i),
                     elevation: 5.0,
                   );
                 }),
           ),
           body: Container(color: colorData.colors),
    );
  }
}

//class ProviderColorPage extends StatelessWidget {
////  @override
////  Widget build(BuildContext context) {
////    //final colorData = Provider.of<ColorProvider>(context);
////    return Scaffold(
////            appBar: AppBar(title: Text('Provider App')),
////            drawer: Drawer(
////              child: ListView.builder(
////                  itemCount: viewModel.colors.length,
////                  itemBuilder: (BuildContext ctx, int i) {
////                    return RaisedButton(
////                      color: viewModel.colors[i],
////                      onPressed: () => viewModel.changeColor(viewModel.colors[i]),
////                      elevation: 5.0,
////                    );
////                  }),
////            ),
////            body: Container(color: viewModel.currentColor),
////
////
////    );
//  }

