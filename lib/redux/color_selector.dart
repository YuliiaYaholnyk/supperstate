
import 'package:redux/redux.dart';
import 'package:superStatePractice/redux/actions.dart';
import 'model.dart';
import 'package:flutter/material.dart' as Col;

class ColorSelector{

  static void Function(Col.Color color) getChangeColorFunction(Store<AppState> store) {
    return (Col.Color color) => store.dispatch(ChangeColorAction(color));
  }

  static Col.Color getCurrentColor(Store<AppState> store) {
    return  store.state.colorState.currentColor;
  }

  static List<Col.Color> getListColors(Store<AppState> store) {
    return  store.state.colorState.colors;
  }


}