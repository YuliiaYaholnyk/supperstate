import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'color_view_model.dart';
import 'model.dart';


class ColorPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ColorViewModel>(
        converter: ColorViewModel.fromStore,
        builder: (BuildContext context, ColorViewModel viewModel) {
          return Scaffold(
            appBar: AppBar(title: Text('Redux App')),
            drawer: Drawer(
              child: ListView.builder(
                  itemCount: viewModel.colors.length,
                  itemBuilder: (BuildContext ctx, int i) {
                    return RaisedButton(
                      color: viewModel.colors[i],
                      onPressed: () => viewModel.changeColor(viewModel.colors[i]),
                      elevation: 5.0,
                    );
                  }),
            ),
            body: Container(color: viewModel.currentColor),
          );
        }

    );
  }
}
