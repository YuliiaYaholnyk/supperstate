import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:superStatePractice/redux/actions.dart';

class AppState {
  final ColorState colorState;

  AppState({
    @required this.colorState,
  });

  factory AppState.initial() => AppState(
    colorState: ColorState.initial(),
      );

  static AppState reducer(AppState state, dynamic action) {
    print('AppState Reducer${action.runtimeType}');
    print(action.runtimeType);
    return AppState(
      colorState: state.colorState.reducer(action),
    );
  }
}

class Reducer<T> {
  final String TAG = '[Reducer<$T>]';
  HashMap<dynamic, T Function(dynamic)> actions;

  Reducer({
    @required this.actions,
  }) {
    actions.forEach((key, value) {
      if (value == null) throw ('All Functions must be initialize');
    });
  }

  T updateState(dynamic action, T state) {
    if (actions.containsKey(action.runtimeType)) {
      return actions[action.runtimeType](action);
    }
    return state;
  }
}

class ColorState {
  static const String TAG = 'ColorState';
  final Color currentColor;
  final List<Color> colors;

  ColorState({
    @required this.currentColor,
    @required this.colors,
  });

  factory ColorState.initial() => ColorState(
        colors: [
          Colors.blue,
          Colors.green,
          Colors.orange,
          Colors.pinkAccent,
          Colors.purple,
          Colors.deepPurple,
        ],
        currentColor: Colors.blue,
      );

  ColorState reducer(dynamic action) {
    print('$TAG => Action runtimetype => ${action.runtimeType}');
    return Reducer<ColorState>(
      actions: HashMap.from({
        ChangeColorAction: (dynamic action) => changeColor((action as ChangeColorAction).color),
      }),
    ).updateState(action, this);
  }

  ColorState changeColor(Color color) {
    return ColorState(currentColor: color, colors: colors);
  }

}
