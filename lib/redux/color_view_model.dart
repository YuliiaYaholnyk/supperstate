import 'package:redux/redux.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:superStatePractice/redux/color_selector.dart';

import 'model.dart';

class ColorViewModel {
  final Color currentColor;
  final void Function(Color color) changeColor;
  final List<Color> colors;


  ColorViewModel({@required this.currentColor,@required this.changeColor,@required this.colors});

  static ColorViewModel fromStore(Store<AppState> store){
    print('VM');
    return ColorViewModel(
      currentColor: ColorSelector.getCurrentColor(store),
      changeColor: ColorSelector.getChangeColorFunction(store),
      colors: ColorSelector.getListColors(store),
    );
  }



}