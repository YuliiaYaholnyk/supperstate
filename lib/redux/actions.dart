import 'model.dart';
import 'package:flutter/material.dart';

class ChangeColorAction{
  final Color color;

  ChangeColorAction(this.color);

}

