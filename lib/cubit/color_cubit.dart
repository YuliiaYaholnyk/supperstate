import 'package:flutter/material.dart';
import 'package:cubit/cubit.dart';


class ColorCubit extends Cubit<List<Color>>{
  ColorCubit() : super([
    Colors.blue,
    Colors.green,
    Colors.orange,
    Colors.pinkAccent,
    Colors.purple,
    Colors.deepPurple,
  ]);

  @override
  void onTransition(Transition<List<Color>> transition) {
    // TODO: implement onTransition
    super.onTransition(transition);
  }

}