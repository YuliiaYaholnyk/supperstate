import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class ColorScopedModel extends Model {
  var _color = Colors.black38;

  Color get color => _color;
  List<Color> colorsList = [
    Colors.blue,
    Colors.green,
    Colors.orange,
    Colors.pinkAccent,
    Colors.purple,
    Colors.deepPurple,
  ];

  void changeColor(int index) {
    _color = colorsList[index];
    notifyListeners();
  }
}
