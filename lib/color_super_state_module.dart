import 'package:flutter_super_state/flutter_super_state.dart';
import 'package:flutter/material.dart';

class ColorModule extends StoreModule {
  var _color = Colors.black38;

  Color get color => _color;
  List<Color> colorsList = [
    Colors.blue,
    Colors.green,
    Colors.orange,
    Colors.pinkAccent,
    Colors.purple,
    Colors.deepPurple,
  ];

  ColorModule(Store store) : super(store);

  void changeColor(int index) {
    setState(() {
      _color = colorsList[index];
    });
  }
}
