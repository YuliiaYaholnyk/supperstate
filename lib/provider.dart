import 'package:flutter/material.dart';

class ColorProvider extends ChangeNotifier {
  var _color = Colors.black38;

  Color get colors => _color;
  List<Color> colorsList = [
    Colors.blue,
    Colors.green,
    Colors.orange,
    Colors.pinkAccent,
    Colors.purple,
    Colors.deepPurple,
  ];

  void changeColor(int index) {
    _color = colorsList[index];
    notifyListeners();
  }
}
