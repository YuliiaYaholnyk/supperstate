import 'package:flutter/material.dart';
import 'package:superStatePractice/bloc/color_block.dart';

import 'color_event.dart';


class ColorPageBlock extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   final _bloc = ColorBlock();
    return StreamBuilder(
      stream: _bloc.color,
      initialData: Colors.black38,
      builder: (BuildContext context, AsyncSnapshot<Color> snapshot){
        return Scaffold(
          appBar: AppBar(title: Text('Block App')),
          drawer: Drawer(
            child: ListView.builder(
                itemCount: _bloc.colorsList.length,
                itemBuilder: (BuildContext ctx, int i) {
                  return RaisedButton(
                    color:  _bloc.colorsList[i],
                    onPressed: () => _bloc.colorEventSink.add(ChangeColorEvent(i)),
                    elevation: 5.0,
                  );
                }),
          ),
          body: Container(color: snapshot.data),
        );
      }
    );
  }
}
