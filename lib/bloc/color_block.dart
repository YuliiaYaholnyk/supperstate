import 'dart:async';
import 'package:flutter/material.dart';

import 'color_event.dart';

class ColorBlock {
  Color _color = Colors.black38;

  // Color get color => _color;
  List<Color> colorsList = [
    Colors.blue,
    Colors.green,
    Colors.orange,
    Colors.pinkAccent,
    Colors.purple,
    Colors.deepPurple,
  ];

  final _colorStateController = StreamController<Color>();

  StreamSink<Color> get _inColor => _colorStateController.sink;

  Stream<Color> get color => _colorStateController.stream;

  final _colorEventController = StreamController<ColorEvent>();

  Sink<ColorEvent> get colorEventSink => _colorEventController.sink;

  ColorBlock() {
    _colorEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(ColorEvent event) {
    if (event is ChangeColorEvent) {
      _color = colorsList[event.index];
      _inColor.add(_color);
    }

  }

  void dispose(){
    _colorEventController.close();
    _colorStateController.close();
  }
}
